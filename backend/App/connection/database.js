const env = require('../environs');
const Pool = require('pg').Pool;

const pool = new Pool({
    database: env.db_name,
    host: env.db_host,
    port: env.db_port,
    user: env.db_user,
    password: env.db_pass
});

module.exports = pool;