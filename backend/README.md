# Thesis Management Backend

This is an [Express.js](https://expressjs.com/) project.

## Getting Started

- Please run the following command

    ```bash
    git clone https://github.com/SDP-CEDT-2023/thesis_management_backend.git
 
    cd thesis_management_backend
 
    npm install
 
    node server.js
    ```

- Don't forget to create .env before using `node server.js`
- Progression update page is available on home page (/) and (/api)
