const pool = require('../connection/database');
const queries = require('../connection/queries');
const errorHandler = require('../middleware/errorHandler');
const check = require('../middleware/validatorMiddleware');

const get_AllProfessor = async (req, res, next) => {
  try {
    const results = await pool.query(queries.get_AllProfessor);
    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
};

const get_ProfessorByName = async (req, res, next) => {
  try {
    const fromData = req.body;
    const { f_name, l_name } = fromData;
    if (!check.isExpectedString(f_name) || !check.isExpectedString(l_name)) {
      return res.status(400).json({ error: 'Invalid input!' });
    }

    const results = await pool.query(queries.get_ProfessorByName, [f_name, l_name]);

    if (results.rowCount == 0) {
      return res.status(404).json({ error: 'Professor not found! ' })
    }

    return res.status(200).json(results.rows)

  } catch (error) {
    next(error);
  }
};

const get_AllProfessorWithRoomStatus = async (req, res, next) => {
  try {
    if (!check.isExpectedNumber(req.params.year)) {
      return res.status(400).json({ error: 'Invalid input! id must be an integer.' });
    }

    const year = parseInt(req.params.year);
    const professors = await pool.query(queries.get_AllProfessor);
    const profinpresentroom = (await pool.query(`SELECT prof_id FROM profinpresentroom WHERE year = ${year}`));
    const transformedData = {};

    professors.rows.forEach(async professor => {
      transformedData[professor.id] = {
        id: professor.id,
        name: professor.name,
        room_status: true
      }
    });

    profinpresentroom.rows.forEach(async professor => {
      if (transformedData[professor.prof_id]) {
        transformedData[professor.prof_id].room_status = false;
      }
    });
    return res.status(200).json(Object.values(transformedData))

  } catch (error) {
    next(error);
  }
};

module.exports = {
  get_AllProfessor,
  get_ProfessorByName,
  get_AllProfessorWithRoomStatus,
}
