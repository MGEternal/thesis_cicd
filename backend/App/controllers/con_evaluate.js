const { query } = require('express');
const pool = require('../connection/database');
const queries = require('../connection/queries');
const errorHandler = require('../middleware/errorHandler');
const check = require('../middleware/validatorMiddleware');

function isExpectedBody(body, isView = false, isEdit = false) {
    let commonChecks = [
        check.isExpectedScore(body.project_title),
        check.isExpectedScore(body.research),
        check.isExpectedScore(body.design),
        check.isExpectedScore(body.develop),
        check.isExpectedScore(body.slide),
        check.isExpectedScore(body.presentation),
        check.isExpectedScore(body.nonverbal),
        check.isExpectedScore(body.content),
        check.isExpectedScore(body.style),
        check.isExpectedScore(body.perfection),
        check.isExpectedScore(body.demonstration),
        check.isExpectedGrade(body.grade),
        check.isExpectedNumber(body.professor_id),
        check.isExpectedNumber(body.project_id)
    ];

    if (isView) {
        commonChecks = [
            check.isExpectedNumber(body.professor_id),
            check.isExpectedNumber(body.project_id)
        ];
    }

    if (isEdit) {
        commonChecks = [
            check.isExpectedNumber(body.professor_id),
            check.isExpectedNumber(body.project_id),
            check.isExpectedArray(body.score)
        ];
    }

    return commonChecks.every((bool) => bool === true);
};


const get_AverageScoreByProjectID = async (req, res, next) => {
    try {

        if (!check.isExpectedNumber(req.params.id)) {
            return res.status(400).json({ error: 'Invalid input! id must be an integer.' });
        }
        const id = parseInt(req.params.id);

        const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${id}`);
        if (isProJExist.rowCount == 0) {
            return res.status(404).json({ message: "project not found!", data: isProJExist.rows })
        }

        const profs_id = []
        const author = isProJExist.rows[0].author
        const present_id = isProJExist.rows[0].presentation

        // const isPresentExist = await pool.query(`SELECT year FROM presentation WHERE id = ${present_id}`);
        // const education_year = isPresentExist.rows[0].year;
        const ProfInPresentRoom = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE present_id = ${present_id}`);

        profs_id.push(author)
        ProfInPresentRoom.rows.forEach(async prof_id => {
            if (!profs_id.includes(Object.values(prof_id)[0])) {
                profs_id.push(Object.values(prof_id)[0]);
            }
        });

        const EvaluateInProJ = await pool.query(`SELECT * FROM evaluate_score WHERE project_id = ${id}`)


        const StudentsInProject = await pool.query(`SELECT id, CONCAT(f_name, ' ', l_name) AS name, score FROM student WHERE project_id = ${id}`);
        const std_score = StudentsInProject.rows;

        const formattedAverage = {
            project_title: 0,
            research: 0,
            design: 0,
            develop: 0,
            slide: 0,
            presentation: 0,
            nonverbal: 0,
            content: 0,
            style: 0,
            perfection: 0,
            demonstration: 0,
            grade: 0,
        };

        const gradeMapping = {
            'A+': 95,
            'A': 90,
            'A-': 85,
            'B+': 80,
            'B': 75,
            'B-': 70,
            'C+': 65,
            'C': 60,
            'C-': 55,
            'D+': 50,
            'D': 45,
            'D-': 40,
            'F': 0
        };

        const comment = []
        EvaluateInProJ.rows.forEach(async evaluated => {
            for (const category in formattedAverage) {
                formattedAverage[category] += evaluated[category];
            }
            comment.push(evaluated.comment)
        });

        for (const category in formattedAverage) {
            formattedAverage[category] = Math.floor(formattedAverage[category] / EvaluateInProJ.rows.length);
        }

        let total_grade = 0
        EvaluateInProJ.rows.forEach(async evaluated => {
            const grade = gradeMapping[evaluated.grade];
            if (grade !== undefined) {
                total_grade += grade
            }
        });

        total_grade = Math.floor(total_grade / EvaluateInProJ.rows.length);

        let averageGradeLetter = 'F'; // Default to 'F' if the average is below any defined grade
        for (const gradeLetter in gradeMapping) {
            if (total_grade >= gradeMapping[gradeLetter]) {
                averageGradeLetter = gradeLetter;
                break;
            }
        }
        formattedAverage.grade = averageGradeLetter;

        let submited_count = 0;
        const submited_profs = []
        const unsubmit_profs = []

        EvaluateInProJ.rows.forEach(async evaluated => {
            if (profs_id.includes(evaluated.professor_id)) {
                submited_count += 1;
                submited_profs.push(evaluated.professor_id);
            }
        });

        const check_unsubmit = profs_id.map(async prof_id => {
            if (!submited_profs.includes(prof_id)) {
                // console.log(prof_id)
                const results = await pool.query(`SELECT CONCAT(f_name, ' ', l_name) AS name FROM professor WHERE id = ${prof_id}`);
                unsubmit_profs.push(results.rows[0].name);
            }
        });


        Promise.all(check_unsubmit).then(() => {

            const formattedData = {}
            formattedData[id] = {
                prof_in_room: profs_id.length,
                submited_form: submited_count,
                not_yet_submit: unsubmit_profs,
                student_score: std_score,
                average: formattedAverage,
                comment: comment
            }
            return res.json(Object.values(formattedData))
        })
            .catch((error) => {
                next(error)
            });

    } catch (error) {
        next(error);
    }
};

const create_EvaluateScore = async (req, res, next) => {
    try {
        const fromData = req.body;
        // if (true) {
        if (isExpectedBody(fromData)) {
            const { project_title, research, design, develop, slide, presentation, nonverbal, content, style, perfection, demonstration, grade, comment, professor_id, project_id } = fromData;

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const profs_id = []
            const present_id = isProJExist.rows[0].presentation
            const author = isProJExist.rows[0].author

            const ProfInPresentRoom = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE present_id = ${present_id}`);

            profs_id.push(author)
            ProfInPresentRoom.rows.forEach(async prof_id => {
                if (!profs_id.includes(Object.values(prof_id)[0])) {
                    profs_id.push(Object.values(prof_id)[0]);
                }
            });

            if (professor_id != author) {
                const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id = ${professor_id} AND present_id = ${present_id}`);
                if (isProfInRoomExist.rowCount == 0) {
                    return res.status(400).json({ message: "professor not exist in this room!", data: isProfInRoomExist.rows })
                }
            }

            const isEvaluateAlreadyExist = await pool.query(`SELECT * FROM evaluate_score WHERE professor_id = ${professor_id} AND project_id = ${project_id}`)
            if (isEvaluateAlreadyExist.rowCount) {
                return res.status(400).json({ message: "professor already have evaluate score!", data: isEvaluateAlreadyExist.rows })
            }

            const results = await pool.query(`  INSERT INTO evaluate_score (
                                                project_title, research, design, develop, slide, presentation, nonverbal,
                                                content, style, perfection, demonstration, grade, comment, professor_id, project_id) 
                                                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)`, [
                project_title, research, design, develop, slide, presentation, nonverbal,
                content, style, perfection, demonstration, grade, comment, professor_id, project_id
            ]);

            const EvaluateInProJ = await pool.query(`SELECT * FROM evaluate_score WHERE project_id = ${project_id}`)

            let submited_count = 0;
            EvaluateInProJ.rows.forEach(evaluated => {
                if (profs_id.includes(evaluated.professor_id)) {
                    submited_count += 1;
                }
            });
            // return res.send(submited_count)

            if (submited_count == profs_id.length) {
                const update_phase = pool.query(`UPDATE project SET phase = 'Done'::phase WHERE id = ${project_id} `)
            }

            return res.status(201).json({ message: 'Data received successfully!' });

        } else {
            return res.status(400).json({ message: 'Incomplete data or invalid input!' });
        }

    } catch (error) {
        next(error)
    }
};

const edit_EvaluateScore = async (req, res, next) => {
    try {
        const fromData = req.body;
        // if (true) {
        if (isExpectedBody(fromData)) {
            const { project_title, research, design, develop, slide, presentation, nonverbal, content, style, perfection, demonstration, grade, comment, professor_id, project_id } = fromData;

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const present_id = isProJExist.rows[0].presentation
            const author = isProJExist.rows[0].author

            if (professor_id != author) {
                const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id = ${professor_id} AND present_id = ${present_id}`);
                if (isProfInRoomExist.rowCount == 0) {
                    return res.status(400).json({ message: "professor not exist in this room!", data: isProfInRoomExist.rows })
                }
            }

            const isEvaluateAlreadyExist = await pool.query(`SELECT * FROM evaluate_score WHERE professor_id = ${professor_id} AND project_id = ${project_id}`)
            if (isEvaluateAlreadyExist.rowCount == 0) {
                return res.status(400).json({ message: "evaluate score not found!", data: isEvaluateAlreadyExist.rows })
            }

            const results = await pool.query(`  UPDATE evaluate_score 
                                                SET project_title = $1, research = $2, design = $3, develop = $4,
                                                slide = $5, presentation = $6, nonverbal = $7, content = $8,
                                                style = $9, perfection = $10, demonstration = $11, grade = $12,
                                                comment = $13
                                                WHERE professor_id = $14 AND project_id = $15`, [
                project_title, research, design, develop, slide, presentation, nonverbal,
                content, style, perfection, demonstration, grade, comment, professor_id, project_id
            ]);

            return res.status(200).json({ message: 'Data received successfully!' });

        } else {
            return res.status(400).json({ message: 'Incomplete data or invalid input!' });
        }

    } catch (error) {
        next(error)
    }
};

const get_EvaluateScore = async (req, res, next) => {
    try {
        const fromData = req.body;
        // if (true) {
        if (isExpectedBody(fromData, isView = true)) {

            const { professor_id, project_id } = fromData;

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const present_id = isProJExist.rows[0].presentation
            const author = isProJExist.rows[0].author

            if (professor_id != author) {
                const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id = ${professor_id} AND present_id = ${present_id}`);
                if (isProfInRoomExist.rowCount == 0) {
                    return res.status(400).json({ message: "professor not exist in this room!", data: isProfInRoomExist.rows })
                }
            }

            const isEvaluateAlreadyExist = await pool.query(`SELECT * FROM evaluate_score WHERE professor_id = ${professor_id} AND project_id = ${project_id}`)
            if (isEvaluateAlreadyExist.rowCount == 0) {
                return res.status(400).json({ message: "evaluate score not found!", data: isEvaluateAlreadyExist.rows })
            }

            return res.status(200).json({ message: "professor's evaluate score!", data: isEvaluateAlreadyExist.rows })

        } else {
            return res.status(400).json({ message: 'Incomplete data or invalid input!' });
        }

    } catch (error) {
        next(error)
    }
};

const edit_StudentScore = async (req, res, next) => {
    try {
        const fromData = req.body;
        // if (true) {
        if (isExpectedBody(fromData, isEdit = true)) {
            const { professor_id, project_id, score } = fromData;    // [{std_id: score}]

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const author = isProJExist.rows[0].author
            if (professor_id != author) {
                return res.status(400).json({ message: "professor not a auther of this project!", data: isProJExist.rows })
            }

            const queryStudentExistInProJ = score.map(async student => {
                const isStudentExistInProJ = await pool.query(`SELECT id FROM student WHERE id = ${student.id} AND project_id = ${project_id}`);
                if (isStudentExistInProJ.rowCount == 0) {
                    return res.status(400).json({ message: "student not exist in this project!", data: isStudentExistInProJ.rows })
                }
            });

            Promise.all(queryStudentExistInProJ)
                .then(() => {
                    score.forEach(async student => {
                        const update_score = await pool.query(`UPDATE student SET score = ${student.score} WHERE id = ${student.id}`);
                    });
                    return res.status(200).json({ message: 'Data received successfully!' });
                })
                .catch((error) => {
                    next(error)
                });

        } else {
            return res.status(400).json({ message: 'Incomplete data or invalid input!' });
        }

    } catch (error) {
        next(error)
    }
};


const get_StudentScore = async (req, res, next) => {
    try {
        const fromData = req.body;
        if (true) {
            // if (isExpectedBody(fromData, isView = true)) {
            const { professor_id, project_id } = fromData;

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const author = isProJExist.rows[0].author
            if (professor_id != author) {
                return res.status(400).json({ message: "professor not a auther of this project!", data: isProJExist.rows });
            }

            const StudentsInProject = await pool.query(`SELECT id, CONCAT(f_name, ' ', l_name) AS name, SUBSTRING(username, 1, 8) AS std_id, score FROM student WHERE project_id = ${project_id}`);
            return res.status(200).json(StudentsInProject.rows);
        }

    } catch (error) {
        next(error)
    }
};

const get_ProfInPresentRoom = async (req, res, next) => {
    try {
        const fromData = req.body;
        if (isExpectedBody(fromData, isView = true)) {
            const { professor_id, project_id } = fromData;

            const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
            if (isProfExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
            }

            const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${project_id}`);
            if (isProJExist.rowCount == 0) {
                return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
            }

            const present_id = isProJExist.rows[0].presentation
            const author = isProJExist.rows[0].author

            if (professor_id != author) {
                const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id = ${professor_id} AND present_id = ${present_id}`);
                if (isProfInRoomExist.rowCount == 0) {
                    return res.status(400).json({ message: "professor not exist in this room!", data: isProfInRoomExist.rows })
                }
                return res.status(200).json({ role: "examiner" });
            }
            return res.status(200).json({ role: "author" });

        } else {
            return res.status(400).json({ message: 'Incomplete data or invalid input!' });
        }

    } catch (error) {
        next(error);
    }
};

module.exports = {
    get_AverageScoreByProjectID,
    create_EvaluateScore,
    edit_EvaluateScore,
    get_EvaluateScore,
    edit_StudentScore,
    get_StudentScore,
    get_ProfInPresentRoom,
};