class AppError extends Error {
    constructor(message, statusCode) {
        super(message);
        this.statusCode = statusCode;
        this.isOperational = true;
        Error.captureStackTrace(this, this.constructor);
    }
}

class BadRequestError extends AppError {
    constructor(message = 'Bad request.') {
        super(message, 400);
    }
}

class NotFoundError extends AppError {
    constructor(message = 'Resource not found.') {
        super(message, 404);
    }
}

class RequestTimeoutError extends AppError {
    constructor(message = 'Request timeout.') {
        super(message, 408);
    }
}

class InternalServerError extends AppError {
    constructor(message = 'An internal server error occurred.') {
        super(message, 500);
    }
}

module.exports = {
    AppError,
    BadRequestError,
    NotFoundError,
    RequestTimeoutError,
    InternalServerError
}
