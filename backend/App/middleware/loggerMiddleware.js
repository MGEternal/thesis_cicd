var morgan = require('morgan');
const chalk = require('chalk');
var moment = require('moment');
var fs = require('fs')
const rfs = require('rotating-file-stream');

const logfolder = __dirname + '/logs';
try {
  if (!fs.existsSync(logfolder)) {
    fs.mkdirSync(logfolder);
  }
} catch (err) {
  console.error(err);
}

// Create a rotating write stream for daily log rotation
const logstream = rfs.createStream((time, index) => {
  if (!time) return moment().format('DD-MM-YYYY') + '-server.log';
  return moment(time).format('DD-MM-YYYY') + `-server-${index}.log`;
}, {
  interval: '1d', // Rotate daily
  path: logfolder,
});

const TerminalLogger = morgan(function (tokens, req, res) {
  // Custom function to set text color based on HTTP status code
  function coloredStatus(status) {
    if (status >= 200 && status < 300) {
      return chalk.green.bold(status);
    } else if (status >= 300 && status < 400) {
      return chalk.cyan.bold(status);
    } else if (status >= 400 && status < 500) {
      return chalk.yellow.bold(status);
    } else if (status >= 500) {
      return chalk.red.bold(status);
    } else {
      return chalk.bold(status);
    }
  }

  function coloredMethod(method) {
    if (method == 'GET') {
      return chalk.green.bold(method);
    } else if (method == 'POST') {
      return chalk.cyan.bold(method);
    } else if (method == 'PUT') {
      return chalk.yellow.bold(method);
    } else if (method == 'DELETE') {
      return chalk.red.bold(method);
    } else {
      return chalk.bold(method);
    }
  }

  function coloredResponseTime(responseTime) {
    const time = parseInt(responseTime);
    if (time < 100) {
      return chalk.green.bold(responseTime);
    } else if (time >= 100 && time < 200) {
      return chalk.yellow.bold(responseTime);
    } else if (time >= 200 && time < 300) {
      return chalk.red.bold(responseTime);
    } else {
      return chalk.bold(responseTime);
    }
  }


  return [
    '\n',
    chalk.hex('#ff4757').bold('--------------------------------------------'),
    '\n',
    coloredMethod(tokens.method(req, res)),
    coloredStatus(tokens.status(req, res)),
    chalk.hex('#B6B7BA').bold(tokens.url(req, res)),
    '(' + tokens.date(req, res) + ')',
    chalk.blue(tokens['remote-addr'](req, res)),
    chalk.red('from ' + tokens.referrer(req, res)),
    chalk.yellow(tokens['user-agent'](req, res)),
    ' - ',
    coloredResponseTime(tokens['response-time'](req, res) + ' ms'),
    '\n',
  ].join(' ');
});

const FileLogger = morgan('[:date[web]] :remote-addr :remote-user request:":method :url HTTP/:http-version" status::status content-length::res[content-length] ":referrer" ":user-agent" - :response-time ms', { stream: logstream });

module.exports = {
    FileLogger,
    TerminalLogger
}