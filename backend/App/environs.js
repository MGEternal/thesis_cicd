require('dotenv').config();

const env = {
    db_host: process.env.DB_HOST,
    db_name: process.env.DB_NAME,
    db_user: process.env.DB_USER,
    db_pass: process.env.DB_PASSWORD,
    db_port: process.env.DB_PORT,
    sv_port: process.env.SV_PORT,
    dev_mode: process.env.NODE_DEV,
}

module.exports = env;