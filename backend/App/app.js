const env = require('./environs');
const express = require("express");
const app = express();
const IndexRouter = require('./routes/index')
const errorHandler = require('./middleware/errorHandler');
const cors = require('cors')
var bodyParser = require('body-parser')
var timeout = require('connect-timeout')
const logger = require('./middleware/loggerMiddleware');

app.use(logger.FileLogger);
app.use(logger.TerminalLogger);

app.set("view engine", "ejs");
app.set('views', __dirname + '/views');
app.set('view options', { delimiter: '%' });
app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.use('/', IndexRouter);
app.use('/api', IndexRouter);

// for handling errorHandlers 408
// 10 seconds timeout
app.use(timeout(10000));
app.use((req, res, next) => {
  if (!req.timedout) {
    next();
  } else {
    const ERROR = new errorHandler.RequestTimeoutError('408 Request timeout');
    next(ERROR);
    res.status(ERROR.statusCode).json({
      message: ERROR.message
    });
  }
});

// for handling errorHandlers 404
app.use((req, res) => {
  const ERROR = new errorHandler.NotFoundError('404 Not Found');
  res.status(ERROR.statusCode).json({
    message: ERROR.message
  });
});

// for handling errorHandlers 500
app.use((err, req, res, next) => {
    const ERROR = new errorHandler.InternalServerError('500 An internal server errorHandler occurred.');
    console.error(err.stack);
    res.status(ERROR.statusCode).json({
        message: ERROR.message,
        stack: env.dev_mode === 'development' ? err.stack : {}
    });
});

module.exports = app;
