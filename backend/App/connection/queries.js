const get_allPresent = "SELECT * from presentation";
const get_PresentByID = "SELECT * from presentation WHERE id = $1";
const get_ProjectByProfessorID = "SELECT * from project WHERE author = $1";
const get_ProjectByID = "SELECT * from project WHERE id = $1";
const del_PresentByID = "DELETE FROM presentation WHERE id = $1";
const create_Presentation = "INSERT INTO presentation (year, location, date, time) VALUES ($1, $2, $3, $4) RETURNING id";
const get_ProjectByStudentID = "SELECT project.id, name, description, CONCAT(f_name, ' ', l_name) AS author, TO_CHAR(create_at::timestamp, 'DD-MM-YYYY') AS create_at, TO_CHAR(update_at::timestamp, 'DD-MM-YYYY') AS update_at, phase FROM project INNER JOIN professor ON project.author = professor.id WHERE project.id = (select project_id FROM student WHERE id = $1)";
const update_Presentation = "UPDATE presentation SET year = $1, location = $2, date = $3, time = $4 WHERE id = $5";
const get_AllProject = "SELECT * from project"
const get_AllProfessor = "SELECT id, CONCAT(f_name, ' ', l_name) AS name, role from professor"
const get_ProfessorByName = "SELECT id, CONCAT(f_name, ' ', l_name) AS name, role from professor WHERE f_name = $1 AND l_name = $2"
const get_DocumentsByProjectID = "SELECT id, name, submit_round, project_id from documents WHERE project_id = $1"
const get_PresentByStudentID = "SELECT * from presentation WHERE id = (select presentation FROM project WHERE id = (select project_id FROM student WHERE id = $1))"
const get_AllProjectsWithProfessorName = "SELECT project.id, name, description, CONCAT(f_name, ' ', l_name) AS author, TO_CHAR(create_at::timestamp, 'DD-MM-YYYY') AS create_at, TO_CHAR(update_at::timestamp, 'DD-MM-YYYY') AS update_at, phase FROM project INNER JOIN professor ON project.author = professor.id ORDER BY update_at ASC"
const get_AllPresentsInYear = "SELECT presentation.id, presentation.year AS education_year, presentation.location, TO_CHAR(presentation.date::timestamp, 'DD-MM-YYYY') AS date, presentation.time, professor.id AS prof_id, CONCAT(professor.f_name, ' ', professor.l_name) AS prof_name, profinpresentroom.role, project.id AS presenters_id, project.name AS presenters FROM presentation INNER JOIN profinpresentroom ON presentation.id = profinpresentroom.present_id INNER JOIN professor ON profinpresentroom.prof_id = professor.id INNER JOIN project ON presentation.id = project.presentation"

module.exports = {
    get_allPresent,
    get_PresentByID,
    get_ProjectByProfessorID,
    get_ProjectByID,
    del_PresentByID,
    create_Presentation,
    get_ProjectByStudentID,
    update_Presentation,
    get_AllProject,
    get_AllProfessor,
    get_ProfessorByName,
    get_DocumentsByProjectID,
    get_PresentByStudentID,
    get_AllProjectsWithProfessorName,
    get_AllPresentsInYear,
};
