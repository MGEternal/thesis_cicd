const crypto = require('crypto');

const encryptMiddleware = (req, res, next) => {
    const algorithm = 'aes-256-cbc';
    const encryptionKey = 'SDP-CEDT-2023';
    const iv = crypto.randomBytes(16);

    const cipher = crypto.createCipheriv(algorithm, Buffer.from(encryptionKey), iv);

    let encryptedData = '';
    cipher.on('readable', () => {
        const chunk = cipher.read();
        if (chunk) {
            encryptedData += chunk.toString('hex');
        }
    });

    cipher.on('end', () => {
        req.encryptedData = iv.toString('hex') + ':' + encryptedData;
        next();
    });

    cipher.write(req.body);
    cipher.end();
};

module.exports = encryptMiddleware;