const pool = require('../connection/database');
const queries = require('../connection/queries');
const errorHandler = require('../middleware/errorHandler');
const check = require('../middleware/validatorMiddleware');

const get_ProjectByStudentID = async (req, res, next) => {
    try {
        if (!check.isExpectedNumber(req.params.id)) {
            return res.status(400).json({ error: 'invalid input! id must be an integer.' });
        }

        const id = parseInt(req.params.id);
        const results = await pool.query(queries.get_ProjectByStudentID, [id]);

        if (results.rowCount == 0) {
            return res.status(404).json({ error: 'project not found!' });
        }

        return res.status(200).json(results.rows);

    } catch (error) {
        next(error);
    }
};

const get_PresentByStudentID = async (req, res, next) => {
    try {
        if (!check.isExpectedNumber(req.params.id)) {
            return res.status(400).json({ error: 'invalid input! id must be an integer.' });
        }
        
        const id = parseInt(req.params.id);
        const results = await pool.query(queries.get_PresentByStudentID, [id]);

        if (results.rowCount == 0) {
            return res.status(404).json({ error: 'presentation not found!' });
        }

        return res.status(200).json(results.rows);

    } catch (error) {
        next(error);
    }
};

module.exports = {
    get_ProjectByStudentID,
    get_PresentByStudentID,
};