const common = require('mocha/lib/interfaces/common');
const pool = require('../connection/database');
const queries = require('../connection/queries');
const errorHandler = require('../middleware/errorHandler');
const check = require('../middleware/validatorMiddleware');

function isExpectedBody(body, isEdit = false) {
  const commonChecks = [
    check.isExpectedNumber(body.education_year),
    check.isExpectedString(body.location),
    check.isExpectedDate(body.date),
    check.isExpectedTime(body.time),
    check.isExpectedNumber(body.chairman),
    check.isExpectedArray(body.examiners),
    check.isExpectedArray(body.presenters)
  ];

  if (isEdit) {
    commonChecks.push(check.isExpectedNumber(body.id));
  }
  return commonChecks.every((bool) => bool === true);
};

const get_AllPresentations = async (req, res, next) => {
  try {
    const results = await pool.query(queries.get_allPresent);
    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
}

const get_Presentation = async (req, res, next) => {
  try {
    if (!check.isExpectedNumber(req.params.id)) {
      return res.status(400).json({ error: 'Invalid input! id must be an integer.' });
    }

    const id = parseInt(req.params.id);
    const results = await pool.query(queries.get_PresentByID, [id]);

    if (results.rowCount == 0) {
      return res.status(404).json({ error: 'Presentation not found! ' })
    }

    return res.status(200).json(results.rows)

  } catch (error) {
    next(error);
  }
};

const delete_Presentation = async (req, res, next) => {
  try {
    if (!check.isExpectedNumber(req.params.id)) {
      return res.status(400).json({ error: 'Invalid input! id must be an integer.' });
    }

    const id = parseInt(req.params.id);

    const isPresentExist = await pool.query(`SELECT * FROM presentation WHERE id = ${id}`);

    if (isPresentExist.rowCount == 0) {
      return res.status(404).json({ message: "presentation not found!", data: isPresentExist.rows })
    }

    const reset_student_score = await pool.query(`UPDATE student SET score = Null WHERE id IN (
                                                  SELECT student.id from student INNER JOIN project ON student.project_id = project.id WHERE project.presentation = ${id}
    )`);

    const update_phase = await pool.query(` UPDATE project SET phase = 'Presentation'::phase WHERE id IN (
                                            SELECT id from project WHERE presentation = ${id}
    )`);

    const del_evaluate_score = await pool.query(` DELETE FROM evaluate_score WHERE project_id IN (
                                                  SELECT id from project WHERE presentation = ${id}
    )`);

    const presentation = isPresentExist.rows[0];
    const del_profinpresentroom = await pool.query(`DELETE FROM profinpresentroom WHERE year = ${presentation.year} AND present_id = ${presentation.id}`)
    const del_projectinpresentroom = await pool.query(`UPDATE project SET presentation = NULL WHERE presentation = ${id}`);

    const results = await pool.query(queries.del_PresentByID, [presentation.id]);
    return res.status(200).json({ message: 'Presentation has deleted!' });

  } catch (error) {
    next(error);
  }
};

const create_Presentation = async (req, res, next) => {
  try {
    const fromData = req.body;
    // if (true) {
    if (isExpectedBody(fromData)) {
      const { education_year, location, date, time, chairman, examiners, presenters } = fromData;
      const profs_id = [].concat(examiners, chairman)

      const projectWithProfResult = await pool.query(`SELECT * FROM project INNER JOIN professor ON author = professor.id WHERE project.id IN (${presenters.join(", ")})`)
      const projectWithProf = projectWithProfResult.rows

      let checkExist = []
      projectWithProf.forEach(pj => {
        if (profs_id.includes(pj.author)) {
          checkExist.push(`${pj.f_name + " " + pj.l_name} is author of ${pj.name}`)
        }
      });

      if (checkExist.length != 0) {
        return res.status(400).json({ message: checkExist.join(", ") })
      }

      const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id IN (${profs_id.join(", ")}) AND year = ${education_year}`);
      if (isProfInRoomExist.rowCount) {
        return res.status(400).json({ message: "professor already exist in other room!", data: isProfInRoomExist.rows })
      }

      const isProfExist = await pool.query(`SELECT id FROM professor WHERE id IN (${profs_id.join(", ")})`);
      if (isProfExist.rowCount == 0) {
        return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
      }

      const isProJExist = await pool.query(`SELECT presentation FROM project WHERE id IN (${presenters.join(", ")}) AND presentation IS NOT NULL`);
      if (isProJExist.rowCount) {
        return res.status(400).json({ message: "presenter already exist in other room!", data: isProJExist.rows })
      }

      const presentation = await pool.query(queries.create_Presentation, [education_year, location, date, time]);
      const present_id = presentation.rows[0].id

      const add_chairman = await pool.query(`INSERT INTO profinpresentroom (prof_id, present_id, year, role) VALUES (${chairman}, ${present_id}, ${education_year}, 'Chairman');`);
      examiners.forEach(async examiner => {
        const add_examiner = await pool.query(`INSERT INTO profinpresentroom (prof_id, present_id, year, role) VALUES (${examiner}, ${present_id}, ${education_year}, 'Examiner');`);
      });

      presenters.forEach(async presenter => {
        const update_project = await pool.query(`UPDATE project SET presentation = ${present_id} WHERE id = ${presenter}`);
      });

      return res.status(201).json({ message: 'Data received successfully!' });
    }
    else {
      return res.status(400).json({ message: 'Incomplete data or invalid input!' });
    }

  } catch (error) {
    next(error);
  }

};

const edit_Presentation = async (req, res, next) => {
  try {
    const fromData = req.body;
    // if (true) {
    if (isExpectedBody(fromData, isEdit = true)) {
      const { id, education_year, location, date, time, chairman, examiners, presenters } = fromData;
      const profs_id = [].concat(chairman, examiners);

      const projectWithProfResult = await pool.query(`SELECT * FROM project INNER JOIN professor ON author = professor.id WHERE project.id IN (${presenters.join(", ")})`)
      const projectWithProf = projectWithProfResult.rows

      let checkExist = []
      projectWithProf.forEach(pj => {
        if (profs_id.includes(pj.author)) {
          checkExist.push(`${pj.f_name + " " + pj.l_name} is author of ${pj.name}`)
        }
      });

      if (checkExist.length != 0) {
        return res.status(400).json({ message: checkExist.join(", ") })
      }

      const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id IN (${profs_id.join(", ")}) AND year = ${education_year} AND present_id <> ${id}`);
      if (isProfInRoomExist.rowCount) {
        return res.status(400).json({ message: "professor already exist in other room!", data: isProfInRoomExist.rows })
      }

      const isProfExist = await pool.query(`SELECT id FROM professor WHERE id IN (${profs_id.join(", ")})`);
      if (isProfExist.rowCount != profs_id.length) {
        return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
      }

      const isProJExist = await pool.query(`SELECT presentation FROM project WHERE id IN (${presenters.join(", ")}) AND presentation IS NOT NULL AND presentation <> ${id}`);
      if (isProJExist.rowCount) {
        return res.status(400).json({ message: "presenter already exist in other room!", data: isProJExist.rows })
      }
      const update_present = await pool.query(queries.update_Presentation, [education_year, location, date, time, id]);

      const del_profinpresentroom = await pool.query(`DELETE FROM profinpresentroom WHERE present_id = ${id}`)

      const add_chairman = await pool.query(`INSERT INTO profinpresentroom (prof_id, present_id, year, role) VALUES (${chairman}, ${id}, ${education_year}, 'Chairman');`);
      examiners.forEach(async examiner => {
        const add_examiner = await pool.query(`INSERT INTO profinpresentroom (prof_id, present_id, year, role) VALUES (${examiner}, ${id}, ${education_year}, 'Examiner');`);
      });

      await pool.query(`UPDATE project SET presentation = NULL WHERE presentation = ${id}`)

      presenters.forEach(async presenter => {
        const update_project = await pool.query(`UPDATE project SET presentation = ${id} WHERE id = ${presenter}`);
      });

      return res.status(200).json({ message: 'Data received successfully!' });
    }
    else {
      return res.status(400).json({ message: 'Incomplete data or invalid input!' });
    }

  } catch (error) {
    next(error);
  }
};


const get_AllPresentsInYear = async (req, res, next) => {
  try {

    const results = await pool.query(queries.get_AllPresentsInYear);

    const formattedData = {}
    const presentations = results.rows

    presentations.forEach(presentation => {

      const { id, education_year, location, date, time, prof_id, prof_name, role, presenters_id, presenters } = presentation;
      const presentationKey = `${id}_${education_year}_${location}_${date}_${time}`;

      if (!formattedData[presentationKey]) {
        formattedData[presentationKey] = {
          id,
          education_year,
          location,
          date,
          time: time.slice(0, -3),
          chairman: null,
          examiners: [],
          presenters: []
        };
      }
      if (role === 'Chairman') {
        // Set chairman only if it's not already set
        if (!formattedData[presentationKey].chairman) {
          formattedData[presentationKey].chairman = { id: prof_id, name: prof_name };
        }
      } else if (role === 'Examiner') {
        // Avoid duplicate examiners
        const existingExaminer = formattedData[presentationKey].examiners.find(examiner => examiner.id === prof_id);
        if (!existingExaminer) {
          formattedData[presentationKey].examiners.push({ id: prof_id, name: prof_name });
        }
      }

      // Avoid duplicate presenters
      const existingPresenter = formattedData[presentationKey].presenters.find(presenter => presenter.id === presenters_id);
      if (!existingPresenter) {
        formattedData[presentationKey].presenters.push({ id: presenters_id, name: presenters });
      }
    });

    const formattedArray = Object.values(formattedData);
    return res.status(200).json(formattedArray);

  } catch (error) {
    next(error);
  }
};

const get_ProfInPresentRoom = async (req, res, next) => {
  try {
    const fromData = req.body;
    if (true) {
      const { professor_id, project_id } = fromData;

      const isProfExist = await pool.query(`SELECT id FROM professor WHERE id = ${professor_id}`);
      if (isProfExist.rowCount == 0) {
        return res.status(400).json({ message: "professor not found!", data: isProfExist.rows })
      }

      const isProJExist = await pool.query(`SELECT presentation, author FROM project WHERE id = ${project_id}`);
      if (isProJExist.rowCount == 0) {
        return res.status(400).json({ message: "professor not found!", data: isProJExist.rows })
      }

      const present_id = isProJExist.rows[0].presentation
      const author = isProJExist.rows[0].author

      if (professor_id != author) {
        const isProfInRoomExist = await pool.query(`SELECT prof_id FROM profinpresentroom WHERE prof_id = ${professor_id} AND present_id = ${present_id}`);
        if (isProfInRoomExist.rowCount == 0) {
          return res.status(400).json({ message: "professor not exist in this room!", data: isProfInRoomExist.rows })
        }
        return res.status(200).json({ role: "examiner" });
      }
      return res.status(200).json({ role: "author" });

    } else {
      return res.status(400).json({ message: 'Incomplete data or invalid input!' });
    }

  } catch (error) {
    next(error);
  }
};

module.exports = {
  get_AllPresentations,
  get_Presentation,
  delete_Presentation,
  create_Presentation,
  edit_Presentation,
  get_AllPresentsInYear
};
