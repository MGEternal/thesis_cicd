var bodyParser = require('body-parser')
var urlencodedParser = bodyParser.urlencoded({ extended: false })
const { Router } = require('express');
const router = Router();
const con_present = require('../controllers/con_presentation');
const con_project = require('../controllers/con_project');
const con_student = require('../controllers/con_student');
const con_professor = require('../controllers/con_professor');
const con_document = require('../controllers/con_document');
const con_evaluate = require('../controllers/con_evaluate');

router.get('/professors', con_professor.get_AllProfessor);
router.get('/get_profess_by_name', con_professor.get_ProfessorByName);
router.get('/presents', con_present.get_AllPresentations);
router.get('/get_present_by_id/:id', con_present.get_Presentation);
router.get('/get_project_by_profess_id/:id', con_project.get_ProjectByProfessor);
router.get('/get_project_by_id/:id', con_project.get_ProjectByID);
router.delete('/delete_present_by_id/:id', con_present.delete_Presentation);
router.post('/create_present', con_present.create_Presentation);
router.get('/get_project_by_std_id/:id', con_student.get_ProjectByStudentID);
router.get('/projects', con_project.get_AllProjects);
router.put('/edit_present_by_id', con_present.edit_Presentation);
router.get('/get_documents_by_project_id/:id', con_document.get_DocumentsByProjectID);
router.get('/get_present_by_std_id/:id', con_student.get_PresentByStudentID);
router.get('/get_projects_with_profess_name', con_project.get_AllProjectWithProfessorName);
router.get('/get_presents_in_year', con_present.get_AllPresentsInYear);
router.get('/get_professors_with_room_status/:year', con_professor.get_AllProfessorWithRoomStatus);
router.get('/get_average_score_by_project_id/:id', con_evaluate.get_AverageScoreByProjectID);
router.post('/create_evaluate_score', con_evaluate.create_EvaluateScore);
router.put('/edit_evaluate_score', con_evaluate.edit_EvaluateScore);
router.get('/get_project_present_phase/:id', con_project.get_ProjectDetailPresent);
router.post('/get_evaluate_score', con_evaluate.get_EvaluateScore);
router.put('/edit_student_score', con_evaluate.edit_StudentScore);
router.post('/get_student_score', con_evaluate.get_StudentScore);
router.post('/get_prof_in_present_room', con_evaluate.get_ProfInPresentRoom);

module.exports = router;
