const pool = require('../connection/database');
const queries = require('../connection/queries');
const errorHandler = require('../middleware/errorHandler');
const check = require('../middleware/validatorMiddleware');


const get_ProjectByProfessor = async (req, res, next) => {
  try {
    if (!check.isExpectedNumber(req.params.id)) {
      return res.status(400).json({ error: 'Invalid input! id must be an integer.' });
    }

    const id = parseInt(req.params.id);
    const results = await pool.query(queries.get_ProjectByProfessorID, [id]);

    if (results.rowCount == 0) {
      return res.status(404).json({ error: 'Projects not found!' });
    }

    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
};

const get_ProjectByID = async (req, res, next) => {
  try {
    if (!check.isExpectedNumber(req.params.id)) {
      return res.status(400).json({ error: 'invalid input! id must be an integer.' });
    }

    const id = parseInt(req.params.id);
    const results = await pool.query(queries.get_ProjectByID, [id]);

    if (results.rowCount == 0) {
      return res.status(404).json({ error: 'Project not found!' });
    }

    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
};

const get_AllProjects = async (req, res, next) => {
  try {
    const results = await pool.query(queries.get_AllProject);
    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
};

const get_AllProjectWithProfessorName = async (req, res, next) => {
  try {
    const results = await pool.query(queries.get_AllProjectsWithProfessorName);
    return res.status(200).json(results.rows);

  } catch (error) {
    next(error);
  }
};

const get_ProjectDetailPresent = async (req, res, next) => {
  try {

    let resData = {
      project_id: req.params.id,
      project_name: "",
      description: "",
      author: "",
      phase: "",
      create_at: "",
      update_at: "",
      presentation: undefined,
      documents: []
    }


    const detailResult = await pool.query(`SELECT * FROM project WHERE id = ${req.params.id}`)
    if (!detailResult.rowCount) return res.status(404).json({ message: "This Project is not exist" })

    const project = detailResult.rows[0]

    const profResult = await pool.query(`SELECT CONCAT(f_name, ' ', l_name) AS name FROM professor WHERE id = ${project.author}`)

    resData.project_id = project.id
    resData.project_name = project.name
    resData.description = project.description
    resData.author = profResult.rows[0].name
    resData.phase = project.phase
    resData.create_at = project.create_at
    resData.update_at = project.update_at


    if (project.presentation) {

      // Get Present Detail
      const presentResult = await pool.query(`SELECT * FROM presentation WHERE id = ${project.presentation}`)
      const profInRoomResult = await pool.query(`
        SELECT profinpresentroom.*, professor.f_name, professor.l_name, professor.id 
        FROM profinpresentroom 
        INNER JOIN professor 
        ON profinpresentroom.prof_id = professor.id 
        WHERE present_id = ${project.presentation}
      `)

      const presentRoom = presentResult.rows[0]
      const profInRoom = profInRoomResult.rows

      let presentation = {
        location: presentRoom.location,
        date: presentRoom.date,
        time: presentRoom.time,
        chairman: "",
        examiners: [],
      }

      profInRoom.forEach(p => {
        if (p.role == "Chairman") presentation.chairman = p.f_name + ' ' + p.l_name
        else presentation.examiners.push(p.f_name + ' ' + p.l_name)
      })

      resData.presentation = presentation
    }

    const docResult = await pool.query(`SELECT * FROM documents WHERE submit_round = 0 and project_id=${project.id}`)
    const documents = docResult.rows

    resData.documents = documents

    return res.status(200).json(resData)

  } catch (error) {
    next(error)
  }
}

module.exports = {
  get_ProjectByProfessor,
  get_ProjectByID,
  get_AllProjects,
  get_AllProjectWithProfessorName,
  get_ProjectDetailPresent
};
