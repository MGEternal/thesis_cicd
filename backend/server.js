const env = require('./App/environs');
const app = require('./App/app');
var PrettyError = require('pretty-error');
pe = new PrettyError();
pe.start();

const port = env.sv_port || 3000;

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`);
});