const { Router } = require('express');
const router = Router();
const Routers = require('./routes')
const env = require('../environs');

router.use('/api/', Routers);

router.get('/', async function(req, res) {
  try {
    var apis = [
      { name: 'Get All Professors', url: "/api/professors", method: "GET", devstate: "Under Testing" },
      { name: 'Get All Presentations', url: "/api/presents", method: "GET", devstate: "Under Testing" },
      { name: 'Get Presentation by ID', url: "/api/get_present_by_id/{id}", method: "GET", devstate: "Under Testing" },
      { name: 'Get Projects by Professor ID', url: "/api/get_project_by_profess_id/{id}", method: "GET", devstate: "Under Testing" },
      { name: 'Get Project by ID', url: "/api/get_project_by_id/{id}", method: "GET", devstate: "Under Testing" },
      { name: 'Delete Presentation by ID', url: "/api/delete_present_by_id/{id}", method: "DELETE", devstate: "Under Testing" },
      { name: 'Create Presentation', url: "/api/create_present", method: "POST", devstate: "Under Testing" },
      { name: 'Get Project by Student ID', url: "/api/get_project_by_std_id/{id}", method: "GET", devstate: "Under Testing" },
      { name: 'Get All Projects', url: "/api/projects", method: "GET", devstate: "Under Testing" },
      { name: 'Edit Presentation', url: "/api/edit_present_by_id", method: "PUT", devstate: "Under testing" },
      { name: 'Get Professor by Name', url: "/api/get_profess_by_name", method: "GET", devstate: "Under Testing" },
      { name: 'Get Documents by Project ID', url: "/api/get_documents_by_project_id/{id}", method: "GET", devstate: "Under testing" },
      { name: 'Get Presentation by Student ID', url: "/api/get_present_by_std_id/{id}", method: "GET", devstate: "Under testing" },
      { name: 'Get All Project with Professor Name', url: "/api/get_projects_with_profess_name", method: "GET", devstate: "Under testing" },
      { name: 'Get All Presentation in Year', url: "/api/get_presents_in_year", method: "GET", devstate: "Under testing" },
      { name: 'Get All Professors with Room Status', url: "/api/get_professors_with_room_status/{year}", method: "GET", devstate: "Under testing" },
      { name: 'Get Average Score by Project ID', url: "/api/get_average_score_by_project_id/{id}", method: "GET", devstate: "Under testing" },
      { name: 'Create Evaluate Score', url: "/api/create_evaluate_score", method: "POST", devstate: "Under testing" },
      { name: 'Edit Evaluate Score', url: "/api/edit_evaluate_score", method: "PUT", devstate: "Under testing" },
      { name: 'Get Evaluate Score', url: "/api/get_evaluate_score", method: "POST", devstate: "Under testing" },
      { name: 'Get Student Score', url: "/api/get_student_score", method: "POST", devstate: "Under testing" },
      { name: 'Edit Student Score', url: "/api/edit_student_score", method: "PUT", devstate: "Under testing" },
      { name: 'Get Professor in Presentation Room', url: "/api/get_prof_in_present_room", method: "POST", devstate: "Under testing" }

    ]

    if (env.dev_mode === 'development') {
      res.render('index', {
        title: 'Backend Docs',
        apis: apis
      });
    }
    else {
      res.render('index', {
        title: 'Backend Docs',
        apis: [
          { name: 'You currently running on production', url: "??????", method: "??", devstate: "Tested" }
        ]
      });
    }
  }
  catch (err) {
    console.error(err.stack);
    res.status(500).json({
      message: 'An internal server error occurred.'
    });
  }
});

module.exports = router;
