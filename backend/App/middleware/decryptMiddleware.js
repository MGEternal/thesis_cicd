const crypto = require('crypto');

const decryptMiddleware = (req, res, next) => {
  const algorithm = 'aes-256-cbc';
  const encryptionKey = 'SDP-CEDT-2023';

  const [iv, encryptedData] = req.encryptedData.split(':');
  const decipher = crypto.createDecipheriv(algorithm, Buffer.from(encryptionKey), Buffer.from(iv, 'hex'));

  let decryptedData = '';
  decipher.on('readable', () => {
    const chunk = decipher.read();
    if (chunk) {
      decryptedData += chunk.toString('utf8');
    }
  });

  decipher.on('end', () => {
    req.decryptedData = decryptedData;
    next();
  });

  decipher.write(Buffer.from(encryptedData, 'hex'));
  decipher.end();
};

module.exports = decryptMiddleware;