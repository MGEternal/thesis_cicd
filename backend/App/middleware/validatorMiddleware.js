var validator = require('validator');
var isEmail = validator.isEmail;
var isEmpty = validator.isEmpty;
var isInt = validator.isInt;

function isExpectedNumber(number) {
    if (number == null) {
        return false
    }
    else {
        return validator.isInt(number.toString(), { min: 0, max: 2147483647 });
    }
}

function isExpectedString(string) {
    if (string == null) {
        return false
    }
    else {
        const isAlphaEnUs = validator.isAlphanumeric(string, 'en-US', { ignore: ' -_' });
        const isAlphaThTh = validator.isAlphanumeric(string.replace(/[a-zA-Z0-9]/g, ''), 'th-TH', { ignore: ' -_' });

        return isAlphaEnUs || isAlphaThTh;
    }
}

function isExpectedHash(hashstr, algorithm) {
    if (hashstr == null || algorithm == null) {
        return false
    }
    else {
        const algo_lst = new Set([
            'crc32', 'crc32b', 'md4', 'md5',
            'ripemd128', 'ripemd160', 'sha1',
            'sha256', 'sha384', 'sha512',
            'tiger128', 'tiger160', 'tiger192'
        ]);

        if (algo_lst.has(algorithm)) {
            return validator.isHash(hashstr, algorithm);
        }
        else {
            throw new Error("Unexpected algorithm only [ " + Array.from(algo_lst).join(', ') + " ] is allowed.");
        }
    }
}

function isExpectedDate(dateString) {
    if (dateString == null) {
        return false
    }
    else {
        // ISO 8601 format
        // return validator.isISO8601(dateString);
        // "DD-MM-YYYY" format
        return validator.isDate(dateString, { format: 'DD-MM-YYYY' });
    }
}

function isExpectedNumberArray(array) {
    if (array == null) {
        return false
    } else {
        return array.every(function (element) { return isExpectedNumber(element) });
    }
}

function isExpectedArray(array) {
    if (array == null) {
        return false
    }
    else {
        return Array.isArray(array);
    }
}

function isExpectedObject(value) {
    return typeof value === 'object' && value !== null && !Array.isArray(value);
}

function isExpectedTime(timeString) {
    if (timeString == null) {
        return false
    }
    else {
        // "HH:MM" format
        return validator.isTime(
            timeString,
            {
                hourFormat: 'hour24',
                mode: 'default'
            }
        )
    }
}

function isExpectedScore(score) {
    if (score == null) {
        return false
    }
    else {
        return validator.isInt(score.toString(), { min: 1, max: 4 });
    }
}

function isExpectedGrade(grade) {
    if (grade == null) {
        return false
    }
    else {
        const expectedGrades = new Set([
            'A+', 'A', 'A-', 
            'B+', 'B', 'B-', 
            'C+', 'C', 'C-', 
            'D+', 'D', 'D-', 
            'F'
        ]);
        return expectedGrades.has(grade);
    }
}

module.exports = {
    isEmpty,
    isInt,
    isEmail,
    isExpectedDate,
    isExpectedArray,
    isExpectedNumberArray,
    isExpectedObject,
    isExpectedTime,
    isExpectedNumber,
    isExpectedString,
    isExpectedHash,
    isExpectedScore,
    isExpectedGrade,
};